import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;


public class UserRole {

    @Test
    public void UserRole() {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://fortesting.info/");
        driver.findElement(By.cssSelector("#Email")).clear();
        driver.findElement(By.cssSelector("#Email")).sendKeys("User2@mailforspam.com");
        driver.findElement(By.cssSelector("#Password")).clear();
        driver.findElement(By.cssSelector("#Password")).sendKeys("123456");
        driver.findElement(By.cssSelector(".btn-success")).click();

        //Login User Role
        driver.findElement(By.cssSelector("#Email")).clear();
        driver.findElement(By.cssSelector("#Email")).sendKeys("JustUser2@mailforspam.com");
        driver.findElement(By.cssSelector("#Password")).clear();
        driver.findElement(By.cssSelector("#Password")).sendKeys("!1234Qwer");
        driver.findElement(By.cssSelector(".btn-success")).click();

        //1)Log in as just user and check if correct email is displayed in menu bar near “Log out” link
        Assert.assertEquals(driver.findElement(By.cssSelector(".navbar-right>li>a[title=Manage]")).getText(), "JustUser2@mailforspam.com");

        //2)Check if user is able to add any product from pharmacy to Cart:
        driver.findElement(By.cssSelector(".btn-primary[href$=\"1040\"]")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector(".nav-tabs>li>a[href=\"#products\"]")).click();
        driver.findElement(By.cssSelector("tr:nth-child(2) td:nth-child(5) input[value=Buy]")).click();
        driver.findElement(By.cssSelector("a[href=\"/ShoppingCart\"]")).click();
        Assert.assertEquals(driver.findElement(By.cssSelector("tr:nth-child(2) td:nth-child(1)")).getText(), "The magic of Mr. White");

        //3)Check if added product is displayed correctly in Cart (quantity, sum, details)
        Assert.assertEquals(driver.findElement(By.cssSelector("tr:nth-child(2) td:nth-child(3)")).getText(), "1");
        Assert.assertEquals(driver.findElement(By.cssSelector("tr:nth-child(2) td:nth-child(4)")).getText(), "5000.00");
        Assert.assertEquals(driver.findElement(By.cssSelector("tr:nth-child(2) td:nth-child(2)")).getText(), "Mr. White");

        //4)Check if user is able to remove product from Cart
        driver.findElement(By.cssSelector(".btn-danger")).click();
        Assert.assertEquals(driver.findElement(By.cssSelector(".panel-body.text-center")).getText(), "There are no products yet.");

        //5)Check if status of product is changed to paid after clicking on “Buy” button
        driver.findElement(By.cssSelector(".navbar-brand:nth-of-type(2)")).click();
        driver.findElement(By.cssSelector(".btn-primary[href$=\"1040\"]")).click();
        driver.findElement(By.cssSelector(".nav-tabs>li>a[href=\"#products\"]")).click();
        driver.findElement(By.cssSelector("tr:nth-child(2) td:nth-child(5) input[value=Buy]")).click();
        driver.findElement(By.cssSelector(".navbar-right>li>a[href=\"/ShoppingCart\"]")).click();
        Assert.assertEquals(driver.findElement(By.cssSelector("tr:nth-child(2) td:nth-child(6)")).getText(), "Pending");
        driver.findElement(By.cssSelector(".btn-success")).click();
        Assert.assertEquals(driver.findElement(By.cssSelector("tr:nth-child(2) td:nth-child(6)")).getText(), "Paid");

        //6)Check if user is able to log out
        driver.findElement(By.cssSelector(".btn-danger")).click();
        Assert.assertEquals(driver.findElement(By.cssSelector(".panel-body.text-center")).getText(), "There are no products yet.");
        driver.findElement(By.cssSelector("li:nth-of-type(3) a")).click();
        driver.close();
    }
}
