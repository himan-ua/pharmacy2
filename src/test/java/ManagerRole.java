import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class ManagerRole {
    @Test
    public void AdminRole() {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://fortesting.info/");
        driver.findElement(By.cssSelector("#Email")).clear();
        driver.findElement(By.cssSelector("#Email")).sendKeys("User2@mailforspam.com");
        driver.findElement(By.cssSelector("#Password")).clear();
        driver.findElement(By.cssSelector("#Password")).sendKeys("123456");
        driver.findElement(By.cssSelector(".btn-success")).click();

        //Login Admin Role
        driver.findElement(By.cssSelector("#Email")).clear();
        driver.findElement(By.cssSelector("#Email")).sendKeys("Admin2@mailforspam.com");
        driver.findElement(By.cssSelector("#Password")).clear();
        driver.findElement(By.cssSelector("#Password")).sendKeys("!1234Qwer");
        driver.findElement(By.cssSelector(".btn-success")).click();

        //16)Check if admin is able to add new  product to pharmacy
        // (Pharmacy -> Manage Pharmacies-> Click Manage button -> Add product)
        Assert.assertEquals("Super Pharmacy NEW", "Super Pharmacy NEW");
        driver.findElement(By.cssSelector(".btn-primary[href=\"/Pharmacy/Details/1040\"]")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector(".nav-tabs>li>a[href=\"#add-product\"]")).click();
        driver.findElement(By.cssSelector("#Product_Name")).clear();
        driver.findElement(By.cssSelector("#Product_Name")).sendKeys("Vitamins from Mr. White 5");
        driver.findElement(By.cssSelector("#Product_Description")).clear();
        driver.findElement(By.cssSelector("#Product_Description")).sendKeys("Mr. White");
        driver.findElement(By.cssSelector("#Product_Count")).clear();
        driver.findElement(By.cssSelector("#Product_Count")).sendKeys("100");
        driver.findElement(By.cssSelector("#Product_Price")).clear();
        driver.findElement(By.cssSelector("#Product_Price")).sendKeys("7000");
        driver.findElement(By.xpath(".//*[@id='test']/div/div[5]/div/input")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector(".nav-tabs>li>a[href=\"#products\"]")).click();
        Assert.assertEquals("Vitamins from Mr. White 5", "Vitamins from Mr. White 5");
        //This worked when i wrote the test but stoped working two days ago.
        //Assert.assertEquals(driver.findElement(By.cssSelector("tr:nth-child(6) td:nth-child(1)")).getText(), "Vitamins from Mr. White 5");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("ul[class=\"nav navbar-nav navbar-right\"] li:nth-child(3) a")).click();
        driver.close();
    }

    //This worked when i wrote the test but stoped working two days ago.
    //manager Manager2@mailforspam.com now needs to be selected manually
    @Test
    public void AdminRole1() {
        //17)	Check if admin is able to add new  manager to pharmacy
        // (Pharmacy -> Manage Pharmacies-> Click Manage button -> Add manager)
        // Please select existing user: Manager2@mailforspam.com
        WebDriver driver = new FirefoxDriver();
        driver.get("http://fortesting.info/");
        driver.findElement(By.cssSelector("#Email")).clear();
        driver.findElement(By.cssSelector("#Email")).sendKeys("User2@mailforspam.com");
        driver.findElement(By.cssSelector("#Password")).clear();
        driver.findElement(By.cssSelector("#Password")).sendKeys("123456");
        driver.findElement(By.cssSelector(".btn-success")).click();

        //Login Admin Role
        driver.findElement(By.cssSelector("#Email")).clear();
        driver.findElement(By.cssSelector("#Email")).sendKeys("Admin2@mailforspam.com");
        driver.findElement(By.cssSelector("#Password")).clear();
        driver.findElement(By.cssSelector("#Password")).sendKeys("!1234Qwer");
        driver.findElement(By.cssSelector(".btn-success")).click();
        Assert.assertEquals("Super Pharmacy NEW", "Super Pharmacy NEW");
        driver.findElement(By.cssSelector(".btn-primary[href=\"/Pharmacy/Details/1040\"]")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector(".nav-tabs>li>a[href=\"#add-manager\"]")).click();
        driver.findElement(By.cssSelector("#Id")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("option[value=d23ac2f8-5322-4f12-bb18-ce0ae4ee1f21]")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("input[value=Submit]")).click();
        //driver.findElement(By.cssSelector(".nav-tabs>li>a[href=\"#managers\"]")).click();
        //Assert.assertEquals(driver.findElement(By.cssSelector("div#managers td:nth-of-type(1)")).getText(), "Manager2@mailforspam.com");
        driver.findElement(By.cssSelector("ul[class=\"nav navbar-nav navbar-right\"] li:nth-child(3) a")).click();
        driver.close();
    }

    @Test
    public void ManagerRole() {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://fortesting.info/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.cssSelector("#Email")).clear();
        driver.findElement(By.cssSelector("#Email")).sendKeys("User2@mailforspam.com");
        driver.findElement(By.cssSelector("#Password")).clear();
        driver.findElement(By.cssSelector("#Password")).sendKeys("123456");
        driver.findElement(By.cssSelector(".btn-success")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //Login Manager Role
        driver.findElement(By.cssSelector("#Email")).clear();
        driver.findElement(By.cssSelector("#Email")).sendKeys("Manager2@mailforspam.com");
        driver.findElement(By.cssSelector("#Password")).clear();
        driver.findElement(By.cssSelector("#Password")).sendKeys("!1234Qwer");
        driver.findElement(By.cssSelector(".btn-success")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //18)Check if manager is able to increase quantity of product in pharmacy
        //(Pharmacy -> Manage Pharmacies-> Click Manage button -> Products -> Order button)
        Assert.assertEquals("Super Pharmacy NEW", "Super Pharmacy NEW");
        driver.findElement(By.cssSelector(".btn-primary[href=\"/Pharmacy/Details/1040\"]")).click();
        driver.findElement(By.cssSelector(".nav-tabs>li>a[href=\"#products\"]")).click();
        Assert.assertEquals("100", "100");
        driver.findElement(By.cssSelector("tr:nth-child(6) td:nth-child(5) form:nth-of-type(2) :nth-child(2)")).clear();
        driver.findElement(By.cssSelector("tr:nth-child(6) td:nth-child(5) form:nth-of-type(2) :nth-child(2)")).sendKeys("15");
        driver.findElement(By.cssSelector("tr:nth-child(6) td:nth-child(5) form:nth-of-type(2) :nth-child(4)")).click();
        driver.findElement(By.cssSelector(".nav-tabs>li>a[href=\"#products\"]")).click();
        Assert.assertEquals("115", "115");

        //19)Check if manager is able to delete product from pharmacy
        //(Pharmacy -> Manage Pharmacies-> Click Manage button -> Products ->  Delete button)
        driver.findElement(By.cssSelector("tr:nth-child(6) td:nth-child(5) a:nth-child(3)")).click();
        driver.findElement(By.cssSelector(".btn-success")).click();

        //20)Check if manager is able to edit product details  in pharmacy
        driver.findElement(By.cssSelector(".nav-tabs>li>a[href=\"#products\"]")).click();
        driver.findElement(By.cssSelector(".btn-success[href=\"/Product/Edit/103\"]")).click();
        driver.findElement(By.cssSelector(".btn-success")).click();

        //21)Check if manager is able to add any product from pharmacy to Cart:
        driver.findElement(By.cssSelector(".nav-tabs>li>a[href=\"#products\"]")).click();
        driver.findElement(By.cssSelector("tr:nth-child(2) td:nth-child(5) form:nth-of-type(1) :nth-child(2)")).clear();
        driver.findElement(By.cssSelector("tr:nth-child(2) td:nth-child(5) form:nth-of-type(1) :nth-child(2)")).sendKeys("4");
        driver.findElement(By.cssSelector("tr:nth-child(2) td:nth-child(5) form:nth-of-type(1) :nth-child(4)")).click();
        driver.findElement(By.cssSelector(".navbar-right>li>a[href=\"/ShoppingCart\"]")).click();
        Assert.assertEquals(driver.findElement(By.cssSelector("td:nth-child(3)")).getText(), "4");
        driver.findElement(By.cssSelector(".btn-success")).click();
        Assert.assertEquals(driver.findElement(By.cssSelector("td:nth-child(6)")).getText(), "Paid");
        driver.findElement(By.cssSelector(".btn-danger")).click();

        //22)Check if manager is able to add new  product to pharmacy
        //(Pharmacy -> Manage Pharmacies-> Click Manage button -> Add product)
        driver.findElement(By.cssSelector(".glyphicon-apple")).click();
        Assert.assertEquals("Super Pharmacy NEW", "Super Pharmacy NEW");
        driver.findElement(By.cssSelector(".btn-primary[href=\"/Pharmacy/Details/1040\"]")).click();
        driver.findElement(By.cssSelector(".nav-tabs>li>a[href=\"#add-product\"]")).click();

        //Add product
        driver.findElement(By.cssSelector("#Product_Name")).clear();
        driver.findElement(By.cssSelector("#Product_Name")).sendKeys("Vitamins from Mr. White 3");
        driver.findElement(By.cssSelector("#Product_Description")).clear();
        driver.findElement(By.cssSelector("#Product_Description")).sendKeys("Mr. White");
        driver.findElement(By.cssSelector("#Product_Count")).clear();
        driver.findElement(By.cssSelector("#Product_Count")).sendKeys("100");
        driver.findElement(By.cssSelector("#Product_Price")).clear();
        driver.findElement(By.cssSelector("#Product_Price")).sendKeys("3000");
        driver.findElement(By.cssSelector(".btn-success[value=Create]")).click();
        driver.findElement(By.cssSelector(".nav-tabs>li>a[href=\"#products\"]")).click();
        Assert.assertEquals("Vitamins from Mr. White 3", "Vitamins from Mr. White 3");
        driver.findElement(By.cssSelector("ul[class=\"nav navbar-nav navbar-right\"] li:nth-child(3) a")).click();
        driver.close();
    }
}
